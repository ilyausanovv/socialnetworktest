package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    String string;

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args){
        new MainClass().run("", "");
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
    }

    public void parser() throws IOException{

        ArrayList<Author> authors = new ArrayList<>();
        ArrayList<Post> posts = new ArrayList<>();

        FileReader fileReader = new FileReader("\\src\\itis\\socialtest\\resources\\Authors.csv");
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        FileReader fileReaderSecond = new FileReader("\\src\\itis\\socialtest\\resources\\PostDatabase");
        BufferedReader bufferedReaderSecond = new BufferedReader(fileReaderSecond);

        while ((string = bufferedReader.readLine()) != null) {

            String[] fields = string.split(",");
            int id = Integer.parseInt(fields[0]);
            String nickname = fields[1];
            String date = fields[2];
            authors.add(new Author((long) id, nickname, date));

        }

        while ((string = bufferedReaderSecond.readLine()) != null) {

            String[] fieldsSecond = string.split(",");
            int id = Integer.parseInt(fieldsSecond[0]);
            int likesCount = Integer.parseInt(fieldsSecond[1]);
            String date = fieldsSecond[2];
            String content = fieldsSecond[3];
        }

    }
}
